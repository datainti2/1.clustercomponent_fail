import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CompchartComponent } from './compchart.component';

describe('CompchartComponent', () => {
  let component: CompchartComponent;
  let fixture: ComponentFixture<CompchartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CompchartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CompchartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
